import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        data: [{
            name: "Bruce Lee",
            age: 44,
        },
        {
            name: "Bruce Lee",
            age: 25,
        },
        {
            name: "Matthew James",
            age: 15,
        },
        {
            name: "Timothy Jackson",
            age: 51,
        },
        {
            name: "Patrick Green",
            age: 61,
        },
        {
            name: "Ethan Brewer",
            age: 25,
        },
        {
            name: "Eugene Reyes",
            age: 42,
        },
        {
            name: "Alex Hawkins",
            age: 51,
        },
        {
            name: "Ronald Foster",
            age: 12,
        },
        {
            name: "Ronnie Powell",
            age: 24,
        },
        {
            name: "Walter Grand",
            age: 52,
        },
        {
            name: "Justin Rivera",
            age: 23,
        },
        {
            name: "Chris Vargas",
            age: 42,
        },
        {
            name: "Tyler Reyes",
            age: 21,
        },
        {
            name: "Charles Ross",
            age: 64,
        },
        {
            name: "Timothy Guerrero",
            age: 61,
        },
        {
            name: "Adam Hoffman",
            age: 23,
        },
        {
            name: "Chris Diaz",
            age: 12,
        },
        {
            name: "Terry Guerrero",
            age: 61,
        },
        {
            name: "Alan Adams",
            age: 41,
        },
        ],
    },
    getters: {
        arr: (state) => state.data.reverse(),
    },
    mutations: {
        ADDNEWUSER(state, user) {
            console.log(state.data);

            state.data.push(user);
        },
    },
    actions: {
        ADDNEWUSER({ commit }) {
            commit("ADDNEWUSER");
        },
    },
});